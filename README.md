# Emilia Core

[![pipeline status](https://gitlab.com/emilia-system/emilia-core/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-core/commits/master)
[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)

This code only watches some files like /etc/os-release and /etc/issue and alters them to be called Emilia, without the necessity of a fork from the package "filesystem" from Arch Linux. 
This package belongs to the Linux Distribution Emilia, therefore, it's optional to the ones who wishes to install only the common programs from Emilia.

## Installation

Just install the systemd hooks. That's it.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Known problems
 - The kernel is still called Arch Linux, but this requires re-compiling the "Linux" package from Arch, it's unnecessary and duplicates work.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia Core - pt_BR

Este código apenas vigia alguns arquivos como /etc/os-release e /etc/issue e os altera para ser como Emilia, sem a necessidade de fork do pacote "filesystem" do Arch Linux. 
Este pacote pertence a Distribuição Linux Emilia, e portanto é opcional àqueles que desejam instalar os programas comuns da Emilia.

## Instalação

Apenas instale as hooks no systemd. Só isso.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

 - O kernel se chama Arch Linux ainda, porém isso requer re-compilar o pacote "Linux" do Arch, e é desnecessário e duplica o trabalho.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)
