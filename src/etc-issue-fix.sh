#!/bin/bash
 
emilia_core_on_install=0

while getopts 'f' arg; do
    f) emilia_core_on_install=1 ;;
done

emilia_modify_etc_issue(){
    sed -i "s/Arch Linux/Emilia System/" /etc/issue
}

if [ ${emilia_core_on_install} -eq 1 ]; then
    emilia_modify_etc_issue
fi

while inotifywait -q -e modify /etc/issue; do
    emilia_modify_etc_issue
done
