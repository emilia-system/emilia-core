#!/bin/bash

emilia_core_on_install=0

while getopts 'f' arg; do
    f) emilia_core_on_install=1 ;;
done

emilia_modify_os_release(){
    #modify os-release instead of replacing. Emilia is NOT Arch
    sed -i "s/Arch Linux/EmiliaOS/" /usr/lib/os-release
    sed -i "s/Arch Linux/Emilia System/" /usr/lib/os-release
    sed -i "s/arch/emilia/" /usr/lib/os-release
    sed -i "s/0;36/0;33/" /usr/lib/os-release
    
    sed -i "s/\(HOME_URL=\).*$/\1\"<not_yet>\"/" /usr/lib/os-release
    sed -i "s/\(DOCUMENTATION_URL=\).*$/\1\"<not_yet>\"/" /usr/lib/os-release    
    sed -i "s/\(SUPPORT_URL=\).*$/\1\"<not_yet>\"/" /usr/lib/os-release
    sed -i "s/\(BUG_REPORT_URL=\).*$/\1\"<not_yet>\"/" /usr/lib/os-release
    
    if grep -Fxq "ID_LIKE=" /usr/lib/os-release; then
        sed -i "s/\(ID_LIKE=\).*$/\1\"archlinux\"/" /usr/lib/os-release
    else
        echo 'ID_LIKE="archlinux"' >> /usr/lib/os-release
    fi
}

if [ ${emilia_core_on_install} -eq 1 ]; then
    emilia_modify_os_release
fi

while inotifywait -q -e modify /usr/lib/os-release; do
    emilia_modify_os_release
done
